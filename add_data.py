import sqlite3

connection = sqlite3.connect("users.db")
cursor = connection.cursor()

cursor.execute("INSERT INTO users VALUES ('Mohammed', 'qwerty123', 19)")
cursor.execute("INSERT INTO users VALUES ('Jack', 'jack123', 20)")
cursor.execute("INSERT INTO users VALUES ('Ali', 'ali345', 24)")
cursor.execute("INSERT INTO users VALUES ('Musk', 'musk@3', 24)")

connection.commit()